Source: mopidy-local-sqlite
Section: sound
Priority: optional
Maintainer: Stein Magnus Jodal <jodal@debian.org>
Build-Depends: debhelper (>= 10),
               dh-python,
               mopidy (>= 1.0.8-2),
               python,
               python-mock,
               python-pytest,
               python-setuptools,
               python-uritools,
Standards-Version: 4.1.4
Homepage: https://github.com/mopidy/mopidy-local-sqlite
Vcs-Git: https://salsa.debian.org/mopidy-team/mopidy-local-sqlite.git
Vcs-Browser: https://salsa.debian.org/mopidy-team/mopidy-local-sqlite
X-Python-Version: >= 2.7

Package: mopidy-local-sqlite
Architecture: all
Depends: ${misc:Depends},
         ${python:Depends},
Description: Mopidy extension for keeping your local library in SQLite
 Mopidy is a music server which can play music from multiple sources, like your
 local hard drive, radio streams, and from Spotify and SoundCloud.
 .
 This package provides a Mopidy local library extension that uses an SQLite
 database for keeping track of your local media. This extension lets you browse
 your music collection by album, artist, composer and performer, and provides
 full-text search capabilities based on SQLite's FTS modules.
